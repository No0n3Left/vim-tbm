" syntax/tbm.vim

" Set comment groups
syntax match tbmShebang "\v#!*$"
syntax match tbmComment "\v#*$"
highlight default link tbmShebang Comment
highlight default link tbmComment Comment

" Set number groups
syntax match tbmInteger "\v<\d+>"
syntax match tbmFloat "\v<\d+\.\d+>"
syntax keyword tbmFlags BODY_NORMAL BODY_SHIELD BODY_WEAPON BODY_STATIC BODY_UNGRIPPABLE BODY_DQ_DISABLE
syntax keyword tbmShapes sphere box cylinder
syntax keyword tbmMaterial flesh steel
highlight default link tbmInteger Number
highlight default link tbmFloat Float
highlight default link tbmFlags Constant
highlight default link tbmShapes Constant
highlight default link tbmMaterial Constant

" Set structure groups
syntax keyword tbmSection world gamerule player body joint env_obj env_obj_joint
highlight default link tbmSection Structure

" Set keyword groups
syntax keyword tbmBody head breast chest stomach groin r_pecs r_biceps r_triceps l_pecs l_biceps l_triceps r_hand l_hand r_butt l_butt r_thigh l_thigh r_leg l_leg r_foot l_foot
syntax keyword tbmJoint neck chest lumbar abs r_pecs r_shoulder r_elbow l_pecs l_shoulder l_elbow r_wrist l_wrist r_glute l_glute r_hip l_hip r_knee l_knee r_ankle l_ankle
highlight default link tbmBody Keyword
highlight default link tbmJoint Keyword

" Set function groups
syntax keyword tbmCommand version
syntax keyword tbmWorldCommand color friction
syntax keyword tbmGameruleCommand matchframes turnframes flag flags dismemberment fracture disqualification dqtimeout dqflag dismemberthreshold fracturethreshold pointthreshold dojotype dojosize engagedistance engageheight engagerotation engagespace engageplayerpos engageplayerrot damage gravity sumo reactiontime drawwinner maxcontacts
syntax keyword tbmBodyCommand shape sides alt_sides pos rot force thrust color density material flag
syntax keyword tbmJointCommand radius pos axis rot range strength velocity
syntax keyword tbmEnvObjCommand shape pos color rot sides force thrust friction hardness bounce material mass flag
syntax keyword tbmEnvObjJointCommand pos visible axis range stength velocity
highlight default link tbmCommand Function
highlight default link tbmWorldCommand Function
highlight default link tbmGameruleCommand Function
highlight default link tbmBodyCommand Function
highlight default link tbmJointCommand Function
highlight default link tbmEnvObjCommand Function
highlight default link tbmEnvObjJointCommand Function
